(ns auth0-sso.views.home
  (:require
   [auth0-sso.views.util :refer [main]]))

(defn panel []
  [main
   [:h1 "Auth0 SSO Demo in CLJS/Re-Frame"]
   [:p "Click around and get a feel for what the app does, then understand the code."]])
