(ns auth0-sso.views.nav
  (:require
   [re-frame.core :as rf]
   [auth0-sso.auth0 :as auth0]
   [auth0-sso.events :as events]))

(defn ^:private nav-link [event label disabled?]
  (let [style {:float "left", :padding "15px 10px 0 0"}
        attrs {:href "#"
               :on-click #(rf/dispatch event)
               :style style}]
    (if disabled?
      [:li {:style style} label]
      [:li>a attrs label])))

(defn ^:private auth-link [page authenticated?]
  (let [auth-event (if authenticated?
                     [::auth0/logout]
                     [::auth0/login page])
        label (if authenticated? "logout" "login")]
    [nav-link auth-event label]))

(defn ^:private maybe-nav-link [current-page this-page event label]
  [nav-link event label (= current-page this-page)])

(defn menu [page authenticated?]
  [:header
   [:img {:src "https://cdn.auth0.com/website/bob/press/shield-dark.png"
          :alt "Auth0 Logo"
          :width "50"
          :height "50"
          :style {:float "left"
                  :margin "1rem"}}]
   [:nav>ul {:style {:list-style "none"}}
    [maybe-nav-link page :home    [::events/navigate :home   ] "home"]
    [maybe-nav-link page :profile [::events/navigate :profile] "profile"]
    [auth-link page authenticated?]]])
