(ns auth0-sso.views.util)

(defn main [& children]
  (into [:main {:style {:clear "both"}}] children))
