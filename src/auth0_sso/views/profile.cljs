(ns auth0-sso.views.profile
  (:require
   [re-frame.core :as rf]
   [auth0-sso.auth0 :as auth0]
   [auth0-sso.subs :as subs]
   [auth0-sso.views.util :refer [main]]))

(defn panel []
  (let [user-data @(rf/subscribe [::auth0/user-data])
        {:strs [picture email]} user-data]
    [main
     [:h1 (str "Profile for " email)]
     [:img {:src picture
            :alt (str "Gravatar for " email)
            :style {:border-radius "50%"}}]
     [:pre (js/JSON.stringify (clj->js user-data) nil 2)]]))
