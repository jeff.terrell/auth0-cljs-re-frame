(ns auth0-sso.routes
  (:require
    [bidi.bidi :as bidi]
    [pushy.core :as pushy]
    [re-frame.core :as rf]
    [auth0-sso.auth0 :as auth0]
    [auth0-sso.events :as events]))

(defmulti panels identity)
(defmethod panels :default [_]
  [:div "No panel found for this route."])

(def ^:private routes
  (atom
    ["/" {""        :home
          "profile" :profile}]))

(def ^:private protected-page?
  #{:profile})

(defn ^:private parse [url]
  (bidi/match-route @routes url))

(defn ^:private url-for [& args]
  (apply bidi/path-for (into [@routes] args)))

(defn ^:private dispatch [route]
  (let [page (:handler route)]
    (rf/dispatch [::events/set-page page])))

(defonce ^:private history
  (pushy/pushy dispatch parse))

(defn ^:private navigate! [page]
  (pushy/set-token! history (url-for page)))

(defn start! []
  (pushy/start! history))

(rf/reg-fx :navigate
  (fn [[page authenticated?]]
    (if (or authenticated? (not (protected-page? page)))
      (navigate! page)
      (rf/dispatch [::auth0/login page]))))
