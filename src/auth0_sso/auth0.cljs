(ns auth0-sso.auth0
  (:require
   [clojure.string :as str]
   [cljs.core.async :refer [go]]
   [cljs.core.async.interop :refer-macros [<p!]]
   [cljs.reader :refer [read-string]]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [re-frame.core :as rf]
   [re-promise.core]
   ["@auth0/auth0-spa-js" :refer [createAuth0Client]]))

(declare ^:private client)

(defn ^:private create-auth0-client!
  "Return a promise that resolves to the auth0 client instance."
  []
  ;; These values come from the Auth0 management console.
  ;; Create an application (of type SPA) then copy the values here.
  ;; They're not private so no need to keep them out of the repo.
  (let [config #js {:domain "cscc-jeff-sso-test.us.auth0.com"
                    :client_id "wnWQa0gAAHv5CGledpWrMwiaVzpjDZML"}]
    (createAuth0Client config)))

(defn ^:private should-handle-callback?
  []
  (let [query-string js/window.location.search]
    (and (string? query-string)
         (.includes query-string "code=")
         (.includes query-string "state="))))

(defn ^:private handle-error [err stage]
  (let [promise-error? (= :promise-error (some-> err ex-data :error))
        err (if promise-error?
              (ex-cause err)
              err)
        message (.-message err)]
    (js/console.error err)
    (rf/dispatch-sync [::set-error-stage stage])
    (rf/dispatch-sync [::set-error-message message])))

(rf/reg-event-db ::set-error-stage
  (fn-traced [db [_ error-stage]]
    (assoc db ::error-stage error-stage)))

(rf/reg-event-db ::set-error-message
  (fn-traced [db [_ error-message]]
    (assoc db ::error-message error-message)))

(rf/reg-event-db ::set-user-data
  (fn-traced [db [_ user-data]]
    (assoc db ::user-data (js->clj user-data))))

(rf/reg-event-db ::finish-loading
  (fn-traced [db _]
    (assoc db ::loading? false)))

(rf/reg-sub ::error-stage
  (fn [db _]
    (::error-stage db)))

(rf/reg-sub ::error-message
  (fn [db _]
    (::error-message db)))

(rf/reg-sub ::user-data
  (fn [db _]
    (::user-data db)))

(rf/reg-sub ::loading?
  (fn [db _]
    (::loading? db)))

(rf/reg-event-fx ::handle-login-error
  (fn [{:keys [_]} [_ err]]
    {:handle-error [err :login]
     :dispatch [::finish-loading]}))

(rf/reg-fx :logout
  (fn []
    (let [options #js {:returnTo js/window.location.origin}]
      (try
        (.logout client options)
        (catch js/Error err (handle-error err :logout))))))

(rf/reg-fx :handle-error
  (fn [[err stage]]
    (handle-error err stage)))

;;;;
;;;; Public interface follows
;;;;

(defn do-auth!
  "Do everything needed to handle authentication with Auth0.

  When a non-nil value (which must be serializable with `pr-str`) is passed to
  the re-frame ::login event of this namespace, the given make-navigate-event
  function will be called with that value as its only argument. The function
  should return a re-frame event, which will be dispatched if it is not nil."
  [make-navigate-event]
  (go
    (let [stage (atom :create-client)]
      (try
        (defonce client (<p! (create-auth0-client!)))
        (let [callback-result (when (should-handle-callback?)
                                (reset! stage :handle-callback)
                                (<p! (.handleRedirectCallback client)))
              page (some-> callback-result .-appState read-string)
              _ (js/window.history.replaceState #js {}
                                                js/document.title
                                                js/window.location.pathname)
              _ (reset! stage :check-authentication)
              authenticated? (<p! (.isAuthenticated client))]
          (when authenticated?
            _ (reset! stage :get-user-data)
            (let [user-data (<p! (.getUser client))]
              (rf/dispatch-sync [::set-user-data user-data])
              (when-let [navigate-event (some-> page make-navigate-event)]
                (rf/dispatch-sync navigate-event)))))
        (catch js/Error err (handle-error err @stage))
        (finally
          (rf/dispatch [::finish-loading]))))))

(rf/reg-event-fx ::login
  ;; page-after-login:
  ;; - represents the page to redirect to after login
  ;; - is passed to the fn you give to the do-auth! fn to create a re-frame
  ;;   navigate event that connects to your SPA routing/navigation system
  ;; - should be serializable with pr-str (a keyword works)
  ;; - can be nil/unspecified, in which case it represents the root path
  (fn-traced [_ [_ page-after-login]]
    (let [options #js {:redirect_uri js/window.location.origin
                       :appState (pr-str page-after-login)}]
      {:promise {:call #(.loginWithRedirect client options)
                 :on-failure [::handle-login-error]}})))

(rf/reg-event-fx ::logout
  (fn-traced [_ _]
    (let [options #js {:returnTo js/window.location.origin}]
      {:logout nil})))
