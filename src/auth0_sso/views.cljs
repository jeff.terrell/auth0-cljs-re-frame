(ns auth0-sso.views
  (:require
    [re-frame.core :as rf]
    [auth0-sso.auth0 :as auth0]
    [auth0-sso.routes :as routes]
    [auth0-sso.subs :as subs]
    [auth0-sso.views.home :as home]
    [auth0-sso.views.nav :as nav]
    [auth0-sso.views.profile :as profile]))

(defn ^:private spinner []
  [:img {:src "spinner.gif"
         :alt "Loading..."}])

(defn ^:private error-panel [page authenticated? stage message]
  [:<>
   [nav/menu page authenticated?]
   [:h1 {:style {:clear :both}} "Authentication Error"]
   [:div.error {:style {:border "1px solid red"
                        :padding "1rem"
                        :background-color "#ddd"}}
    [:code (pr-str {:stage stage, :message message})]]])

(defmethod routes/panels :home    [_] [   home/panel])
(defmethod routes/panels :profile [_] [profile/panel])

(defn root-view []
  (let [page @(rf/subscribe [::subs/page])
        loading? @(rf/subscribe [::auth0/loading?])
        error-stage @(rf/subscribe [::auth0/error-stage])
        error-message @(rf/subscribe [::auth0/error-message])
        authenticated? (boolean @(rf/subscribe [::auth0/user-data]))]
    (cond
      error-stage [error-panel page authenticated? error-stage error-message]
      loading? [spinner]
      :else [:<>
             [nav/menu page authenticated?]
             [routes/panels page]])))
