(ns auth0-sso.db
  (:require [auth0-sso.auth0 :as auth0]))

(def default-db
  {
   ;; App state
   :page :home

   ;; The following doesn't need to be in the initial app db, but will be added
   ;; by the auth code as needed and is included here for documentation
   ;; purposes.

   ;; Whether the authentication is in progress:
   ::auth0/loading? true

   ;; The profile data for the authenticated user (nil if no authenticated user):
   ::auth0/user-data nil

   ;; The operation that caused the error
   ;; - nil (no error)
   ;; - :create-client
   ;; - :handle-callback
   ;; - :check-authentication
   ;; - :get-user-data
   ;; - :login
   ;; - :logout
   ::auth0/error-stage nil

   ;; The error message:
   ::auth0/error-message nil})
