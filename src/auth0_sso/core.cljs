(ns auth0-sso.core
  (:require
   [reagent.dom :as rdom]
   [re-frame.core :as rf]
   [auth0-sso.auth0 :refer [do-auth!]]
   [auth0-sso.events :as events]
   [auth0-sso.routes :as routes]
   [auth0-sso.views :as views]))

(defn ^:dev/after-load mount-root
  []
  (rf/clear-subscription-cache!)
  (let [root-el (.getElementById js/document "app")]
    (rdom/unmount-component-at-node root-el)
    (rdom/render [views/root-view] root-el)))

(defn init
  []
  (enable-console-print!)
  (routes/start!)
  (rf/dispatch-sync [::events/initialize-db])
  (mount-root)
  (do-auth! (fn [page] [::events/navigate page])))
