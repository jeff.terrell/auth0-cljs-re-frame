(ns auth0-sso.events
  (:require
   [re-frame.core :as rf]
   [auth0-sso.auth0 :as auth0]
   [auth0-sso.db :as db]
   [day8.re-frame.tracing :refer-macros [fn-traced]]))

(rf/reg-event-db ::initialize-db
  (fn-traced [_ _]
    db/default-db))

(rf/reg-event-fx ::navigate
  (fn-traced [{:keys [db]} [_ page]]
    (let [authenticated? (boolean (::auth0/user-data db))]
      {:navigate [(or page :home) authenticated?]})))

(rf/reg-event-db ::set-page
  (fn-traced [db [_ page]]
    (assoc db :page page)))
