(ns auth0-sso.subs
  (:require
    [re-frame.core :as rf]))

(rf/reg-sub ::page
  (fn [db _]
    (:page db)))
